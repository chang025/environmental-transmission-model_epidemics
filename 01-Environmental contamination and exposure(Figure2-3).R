#_______________________________________________________________________________
#                                                                              |
# the code to make theoretical curve                                           |
#______________________________________________________________________________|                               

library(ggplot2)
library(ggpubr)
library(DataCombine) 

Infected <- c( rep(1,101), rep(0, 100))
data <- data.frame(time=seq(0,2, by = 0.01), Infected)

u0.01<-0.01
u100 <- 100
u0.6 <- 0.6
dt <- 0.01

phi0.01 <- u0.01^2/(-1+exp(-u0.01)+u0.01) 
phi0.6 <- u0.6^2/(-1+exp(-u0.6)+u0.6) 
phi100 <- u100^2/(-1+exp(-u100)+u100)
data$hazard_u0.01[1] <- 0
data$hazard_u0.6[1] <- 0
data$hazard_u100[1] <- 0
data$hazard_u0.01_p1[1] <- 0
data$hazard_u0.6_p1[1] <- 0
data$hazard_u100_p1[1] <- 0

data$Exp_u0.01 <- 0
data$Exp_u0.6 <- 0
data$Exp_u100 <- 0 

data$Exp_u0.01p1 <- 0
data$Exp_u0.6p1 <- 0
data$Exp_u100p1 <- 0


for( i in 2:201) {
  
  data$hazard_u0.01[i] <- Infected[i]*(1-exp(-u0.01*dt))*phi0.01/u0.01 +
    exp(-dt*u0.01)*data$hazard_u0.01[i-1]
  
  data$Exp_u0.01[i]<- Infected[i]*(-1+ exp(-u0.01*dt)+ u0.01*dt)/(-1+ exp(-u0.01)+ u0.01)+
    (1-exp(-u0.01*dt))/u0.01*data$hazard_u0.01[i-1] +  data$Exp_u0.01[i-1] 
  
  data$hazard_u0.6[i] <- Infected[i]*(1-exp(-u0.6*dt))*phi0.6/u0.6+
    exp(-dt*u0.6)*data$hazard_u0.6[i-1]
  
  data$Exp_u0.6[i] <- Infected[i]*(-1+ exp(-u0.6*dt)+ u0.6*dt)/(-1+ exp(-u0.6)+ u0.6)+
    (1-exp(-u0.6*dt))/u0.6*data$hazard_u0.6[i-1] +  data$Exp_u0.6[i-1] 
  
  data$hazard_u100[i] <- Infected[i]*(1-exp(-u100*dt))*phi100/u100  +
    exp(-dt*u100)*data$hazard_u100[i-1]
  
  data$Exp_u100[i] <- Infected[i]*(-1+ exp(-u100*dt)+ u100*dt)/(-1+ exp(-u100)+ u100)+
    (1-exp(-u100*dt))/u100*data$hazard_u100[i-1] +  data$Exp_u100[i-1] 
  
  
## When the phi is fixed at 1 
  data$hazard_u0.01_p1[i] <- Infected[i]*(1-exp(-u0.01*dt))/u0.01 +
    exp(-dt*u0.01)*data$hazard_u0.01_p1[i-1]
  
  data$Exp_u0.01p1[i]<- Infected[i]*(-1+ exp(-u0.01*dt)+ u0.01*dt)/(u0.01^2)+
    (1-exp(-u0.01*dt))/u0.01*data$hazard_u0.01_p1[i-1] +  data$Exp_u0.01p1[i-1] 
  
  data$hazard_u0.6_p1[i] <- Infected[i]*(1-exp(-u0.6*dt))/u0.6+
    exp(-dt*u0.6)*data$hazard_u0.6_p1[i-1]
  
  data$Exp_u0.6p1[i]<- Infected[i]*(-1+ exp(-u0.6*dt)+ u0.6*dt)/(u0.6^2)+
    (1-exp(-u0.6*dt))/u0.6*data$hazard_u0.6_p1[i-1] +  data$Exp_u0.6p1[i-1] 
  
  data$hazard_u100_p1[i] <- Infected[i]*(1-exp(-u100*dt))/u100  +
    exp(-dt*u100)*data$hazard_u100_p1[i-1]
  
  data$Exp_u100p1[i]<- Infected[i]*(-1+ exp(-u100*dt)+ u100*dt)/(u100^2)+
    (1-exp(-u100*dt))/u100*data$hazard_u100_p1[i-1] +  data$Exp_u100p1[i-1] 
  
}

Expo <- data.frame(time=seq(0.01,1, by = 0.01), 
                   I = c(rep(1, 100), rep(0, 100)), 
                   E0.01 = data$hazard_u0.01[1:200], 
                   E0.6 = data$hazard_u0.6[1:200],
                   E100 = data$hazard_u100[1:200])
data$Infected[101:201] <- NA

library(dplyr)
library(ggplot2)

breaks = factor(c("Infected invidividual","decay rate= 0.01",
                     "decay rate = 0.6","decay rate = 100"),
                   levels=c("Infected invidividual","decay rate= 0.01",
                            "decay rate = 0.6","decay rate = 100"))
data %>% 
  ggplot(aes(x=time)) + 
  geom_line(aes(x=time, y=Infected, col= "Infected invidividual"), size =1) + 
  geom_line(aes(y=hazard_u0.01, col="decay rate= 0.01"), size =1) + 
  geom_line(aes(y=hazard_u0.6, col ="decay rate = 0.6"), size =1) + 
  geom_line(aes(y=hazard_u100, col = "decay rate = 100"), size =1) + 
  theme_classic() + 
  scale_color_discrete( breaks =breaks)+ 
  labs(x="", col = "")+ 
  scale_y_continuous(limits= c(0, 2), name = " ", 
                     sec.axis = sec_axis(~.*1,
                                         name = "Environmental contamination E(t)"))+
  theme(axis.text = element_text(size=rel(1.1), angle=0), 
        axis.title = element_text(size=rel(1.1),face="bold", angle=0), 
        legend.text = element_text(size=11))-> p2 

data %>% 
  ggplot(aes(x=time)) + 
  geom_line(aes(y=Infected, col= "Infected invidividual"), size =1) + 
  geom_line(aes(y=hazard_u0.01_p1, col="decay rate= 0.01"), size =1) + 
  geom_line(aes(y=hazard_u0.6_p1, col="decay rate = 0.6"), size =1) + 
  geom_line(aes(y=hazard_u100_p1, col = "decay rate = 100"), size =1) + 
  theme_classic() + 
  scale_color_discrete(  breaks =c("Infected invidividual","decay rate= 0.01",
                                   "decay rate = 0.6","decay rate = 100"))+
  labs(x="", col = "")+
  scale_y_continuous(limits= c(0, 2), name = " # of infectious individuals", 
                     sec.axis = sec_axis(~.*1,
                                         name = ""))+
  theme(axis.text=element_text(size=rel(1.1), angle=0), 
        axis.title=element_text(size=rel(1.1),face="bold", angle=0),
        legend.text = element_text(size=11))-> p1 


data %>% 
  ggplot(aes(x=time)) + 
  geom_line(aes(y=Infected, col= "Infected invidividual"), size =1) + 
  geom_line(aes(y=Exp_u0.01p1, col="decay rate= 0.01"), size =1) + 
  geom_line(aes(y=Exp_u0.6p1, col="decay rate = 0.6"), size =1) + 
  geom_line(aes(y=Exp_u100p1, col = "decay rate = 100"), size =1) + 
  theme_classic() + 
  scale_color_discrete(  breaks =c("Infected invidividual","decay rate= 0.01",
                                   "decay rate = 0.6","decay rate = 100"))+

  labs(x="Time(days)", col = "")+
  scale_y_continuous(limits= c(0, 3), name = " # of infectious individuals", 
                     sec.axis = sec_axis(~.*1,
                                         name = ""))+
  theme(axis.text=element_text(size=rel(1.1), angle=0), 
        axis.title=element_text(size=rel(1.1),face="bold", angle=0),
        legend.text = element_text(size=11))-> p3 

data %>% 
  ggplot(aes(x=time)) + 
  geom_line(aes(y=Infected, col= "Infected invidividual"), size =1) + 
  geom_line(aes(y=Exp_u0.01, col="decay rate= 0.01"), size =1) + 
  geom_line(aes(y=Exp_u0.6, col="decay rate = 0.6"), size =1) + 
  geom_line(aes(y=Exp_u100, col = "decay rate = 100"), size =1) + 
  theme_classic() + 
  scale_color_discrete(  breaks =c("Infected invidividual","decay rate= 0.01",
                                   "decay rate = 0.6","decay rate = 100"))+
 
  labs(x="Time(days)", col = "")+
  scale_y_continuous(limits= c(0, 3), name = "", 
                     sec.axis = sec_axis(~.*1,
                        name = "Exposure \n (area under the curve of E(t))"))+
  theme(axis.text=element_text(size=rel(1.1), angle=0), 
        axis.title=element_text(size=rel(1.1),face="bold", angle=0),
        legend.text = element_text(size=11)) +
  geom_point(aes(x=1, y=1), colour="red") -> p4

ggarrange(p1, p2, 
          p3, p4,
          common.legend = TRUE,
          ncol = 2, nrow=2,
          labels =c("A", "B", "C", "D"),
          legend = "right", 
          font.label =  list(size = 15)) -> figure3 # 1000 and 800
#_______________________________________________________________________________
#                                                                              |
#code for figure 2                                                             |
#______________________________________________________________________________|
rm(list = ls())

Infected=c(rep(1,31), rep(0, 30))
data2 <- data.frame(time=seq(0,60, by = 1),
                    Infected) 
u0.3 <- 0.3
dt = 1
phi0.3 <- u0.3^2/(-1+exp(-u0.3)+u0.3) 
data2$hazard_u0.3[1] <- 0
for( i in 2:61) {
  data2$hazard_u0.3[i] <- Infected[i]*(1-exp(-u0.3*dt))*phi0.3/u0.3 +
    exp(-dt*u0.3)*data2$hazard_u0.3[i-1]
  
}
data2 <- InsertRow(data2, NewRow = c(30.00001, 0, 7.348751907), RowNum = 31)
data2$time[1] = 0.00001
data2 <-  InsertRow(data2, NewRow = c(0.00000, 0, 0), RowNum = 1)

data2 %>% 
  ggplot(aes(x= time)) + 
  geom_line(aes( y=hazard_u0.3), size=1, col = "black")+ 
  geom_line(aes( y=Infected), size =1, col = "Red") + 
  theme_classic() + 
  labs(x="Time(days)", y= "Environmental infectivity E(t)", col= "")+
  scale_y_continuous(limits= c(0, 8), name = " # of infectious individuals", 
                     sec.axis = sec_axis(~.*1,
                                         name = "Environmental contamination E(t)"))+
  theme(axis.text=element_text(size=rel(1.1), angle=0), 
        axis.title=element_text(size=rel(1.1),face="bold", angle=0),
        legend.text = element_text(size=11)) -> figure2  # 600 400

